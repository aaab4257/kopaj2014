<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('ProductTableSeeder');
	}

}
class ProductTableSeeder extends Seeder {
	/*
	This is no longer a ProductTableSeeder. This class seeds everything...
	*/
    public function run()
    {
		DB::table('products')->delete();
        DB::table('stocks')->delete();
		DB::table('categories')->delete();


        Stock::create(array(
		'product_id' => 5
		));
		Stock::create(array(
		'product_id' => 6
		));
        Product::create(array(
		'name' => 'Nuttella',
		'category' => 1,
		'description' => 'Ez egy valami.',
		'price' => '1100',
		'action' => '50',
		'stock' => '117',
		'picurl' => 'nutella.png'
		));
		Product::create(array(
		'name' => '10 dkg vegyes gumicukor',
		'category' => 2,
		'description' => 'Ez egy gumicukor.',
		'price' => '900',
		'action' => '10',
		'stock' => '55',
		'picurl' => 'gumicukor.jpg'
		));
		Product::create(array(
		'name' => 'Söröcske',
		'category' => 4,
		'description' => 'Ez egy sör.',
		'price' => '180',
		'action' => '20',
		'stock' => '647',
		'picurl' => 'sor.png'
		));
		
		Category::create(array( 'name' => 'Édességek' ));
		Category::create(array( 'name' => 'Csipszek' ));
		Category::create(array( 'name' => 'Ruhák' ));
		Category::create(array( 'name' => 'Sörök' ));
		Category::create(array( 'name' => 'Tömény italok' ));
    }

}