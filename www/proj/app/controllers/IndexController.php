<?php
class IndexController extends BaseController {

    /**
     * Show the profile for the given user.
     */
    public function showIndex()
    {
        $categories = Category::all();
		$specialofferbig = Product::where('action','>',0)->firstOrFail();
		$specialOffers = Product::where('action','>',0)->get();

        return View::make('index')->with(array(
		'categories' => $categories,
		'specialofferbig' => $specialofferbig,
		'specialOffers' => $specialOffers));
    }

}